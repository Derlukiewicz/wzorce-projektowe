﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	class Program
	{
		static void Main(string[] args)
		{
			Pizzeria wloska = new WloskaPizzeria();
			Pizzeria amerykanska = new AmerykanskaPizzeria();

			Pizza pizza = wloska.ZamowPizza("serowa");
			Console.WriteLine("Eryk zamówił: " + pizza.PobierzNazwe());
			Console.WriteLine();

			pizza = amerykanska.ZamowPizza("serowa");
			Console.WriteLine("Jacek zamówił: " + pizza.PobierzNazwe());

			Console.ReadKey();
		}
	}
}

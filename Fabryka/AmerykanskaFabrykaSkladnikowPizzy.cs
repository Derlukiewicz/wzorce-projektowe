﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public class AmerykanskaFabrykaSkladnikowPizzy : FabrykaSkladnikowPizzy
	{
		public Ciasto UtworzCiasto()
		{
			return new GrubeChrupkieCiasto();
		}

		public Sos UtworzSos()
		{
			return new SosPomidorowy();
		}

		public Ser UtworzSer()
		{
			return new SerMozzarella();
		}

		public List<Warzywa> UtworzWarzywa()
		{
			List<Warzywa> warzywa = new List<Warzywa>() 
			{ 
				new Szpinak(), 
				new Baklazan(), 
				new CzarneOliwki()
			};
			return warzywa;
		}

		public Pepperoni UtworzPepperoni()
		{
			return new PlastryPepperoni();
		}

		public Malze UtworzMalze()
		{
			return new MrozoneMalze();
		}
	}
}

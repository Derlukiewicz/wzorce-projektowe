﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public interface FabrykaSkladnikowPizzy
	{
		Ciasto UtworzCiasto();
		Sos UtworzSos();
		Ser UtworzSer();
		List<Warzywa> UtworzWarzywa();
		Pepperoni UtworzPepperoni();
		Malze UtworzMalze();
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public class WloskaPizzeria : Pizzeria
	{
		public override Pizza UtworzPizza(string pozycja)
		{
			Pizza pizza = null;
			FabrykaSkladnikowPizzy fabrykaSkladnikow = new WloskaFabrykaSkladnikowPizzy();

			switch (pozycja)
			{
				case "serowa" : //return new WloskaSerowaPizza();
					pizza = new SerowaPizza(fabrykaSkladnikow);
					pizza.UstawNazwa("Włoska Pizza Serowa");
					break;
				//case "wegetarianska" : return new WloskaWegetarianskaPizza();
				//	break;
				//case "owoce morza" : return new WloskaOwoceMorzaPizza();
				//	break;
				//case "pepperoni" : return new WloskaPepperoniPizza();
				//	break;
				default: return null;
			}

			return pizza;
		}
	}
}

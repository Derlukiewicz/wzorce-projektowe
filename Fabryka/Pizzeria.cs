﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public abstract class Pizzeria
	{
		public Pizza ZamowPizza(string typ)
		{
			Pizza pizza;
			pizza = UtworzPizza(typ);
			pizza.Przygotowanie();
			pizza.Pieczenie();
			pizza.Krojenie();
			pizza.Pakowanie();
			return pizza;
		}

		public abstract Pizza UtworzPizza(string typ);
	}
}

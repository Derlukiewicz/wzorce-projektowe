﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public class SerowaPizza : Pizza
	{
		private FabrykaSkladnikowPizzy fabrykaSkladnikow;

		public SerowaPizza(FabrykaSkladnikowPizzy fabrykaSkladnikow)
		{
			this.fabrykaSkladnikow = fabrykaSkladnikow;
		}

		public override void Przygotowanie()
		{
			Console.WriteLine("Przygotowanie: " + Nazwa);
			Ciasto_ = fabrykaSkladnikow.UtworzCiasto();
			Console.WriteLine(Ciasto_.toString());
			Sos_ = fabrykaSkladnikow.UtworzSos();
			Console.WriteLine(Sos_.toString());
			Ser_ = fabrykaSkladnikow.UtworzSer();
			Console.WriteLine(Ser_.toString());
		}
	}
}

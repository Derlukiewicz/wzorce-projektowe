﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public class AmerykanskaPizzeria : Pizzeria
	{
		public override Pizza UtworzPizza(string pozycja)
		{
			Pizza pizza = null;
			FabrykaSkladnikowPizzy fabrykaSkladnikow = new AmerykanskaFabrykaSkladnikowPizzy();

			switch (pozycja)
			{
				case "serowa": //return new AmerykanskaSerowaPizza();
					pizza = new SerowaPizza(fabrykaSkladnikow);
					pizza.UstawNazwa("Amerykańska Pizza Serowa");
					break;
				//case "wegetarianska": return new AmerykanskaWegetarianskaPizza();
				//	break;
				//case "owoce morza": return new AmerykanskaOwoceMorzaPizza();
				//	break;
				//case "pepperoni": return new AmerykanskaPepperoniPizza();
				//	break;
				default: return null;
			}

			return pizza;
		}
	}
}

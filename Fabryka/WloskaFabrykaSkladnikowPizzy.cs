﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public class WloskaFabrykaSkladnikowPizzy : FabrykaSkladnikowPizzy
	{
		public Ciasto UtworzCiasto()
		{
			return new CienkieChrupkieCiasto();
		}

		public Sos UtworzSos()
		{
			return new SosMarinara();
		}

		public Ser UtworzSer()
		{
			return new SerReggiano();
		}

		public List<Warzywa> UtworzWarzywa()
		{
			List<Warzywa> warzywa = new List<Warzywa>() 
			{ 
				new Czosnek(), 
				new Cebula(), 
				new Pieczarki(), 
				new CzerwonaPapryka() 
			};
			return warzywa;
		}

		public Pepperoni UtworzPepperoni()
		{
			return new PlastryPepperoni();
		}

		public Malze UtworzMalze()
		{
			return new SwiezeMalze();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public class OwoceMorzaPizza : Pizza
	{
		private FabrykaSkladnikowPizzy fabrykaSkladnikow;

		public OwoceMorzaPizza(FabrykaSkladnikowPizzy fabrykaSkladnikow)
		{
			this.fabrykaSkladnikow = fabrykaSkladnikow;
		}

		public override void Przygotowanie()
		{
			Console.WriteLine("Przygotowanie: " + Nazwa);
			Ciasto_ = fabrykaSkladnikow.UtworzCiasto();
			Sos_ = fabrykaSkladnikow.UtworzSos();
			Ser_ = fabrykaSkladnikow.UtworzSer();
			Malze_ = fabrykaSkladnikow.UtworzMalze();
		}
	}
}

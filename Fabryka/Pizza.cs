﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
	public abstract class Pizza
	{
		protected string Nazwa { get; set; }
		//protected string Ciasto { get; set; }
		protected Ciasto Ciasto_ { get; set; }
		//protected string Sos { get; set; }
		protected Sos Sos_ { get; set; }
		//List<string> dodatki = new List<string>(2);
		//protected List<string> Dodatki
		//{
		//	get { return dodatki; }
		//	set { dodatki = value; }
		//}
		protected List<Warzywa> Warzywa_ { get; set; }
		protected Ser Ser_ { get; set; }
		protected Pepperoni Pepperoni_ { get; set; }
		protected Malze Malze_ { get; set; }

		//public void Przygotowanie()
		//{
		//	Console.WriteLine("Przygotowanie: " + Nazwa);
		//	Console.WriteLine("Wyrabianie cista...");
		//	Console.WriteLine("Dodawanie sosu...");
		//	Console.WriteLine("Dodatki:");
		//	foreach (string item in dodatki)
		//	{
		//		Console.WriteLine(" " + item);
		//	}
		//}

		public abstract void Przygotowanie();

		public virtual void Pieczenie()
		{
			Console.WriteLine("Pieczenie: 25 minut w temperaturze 350 stopni Celsjusza");
		}

		public virtual void Krojenie()
		{
			Console.WriteLine("Krojenie pizzy na 8 kawałków");
		}

		public virtual void Pakowanie()
		{
			Console.WriteLine("Pakowanie pizzy w oficjalne pudełko naszej sieci Pizzerii.");
		}

		public void UstawNazwa(string nazwa)
		{
			this.Nazwa = nazwa;
		}

		public string PobierzNazwe()
		{
			return Nazwa;
		}
	}
}

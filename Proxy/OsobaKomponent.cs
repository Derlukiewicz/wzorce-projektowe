﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    public class OsobaKomponent : IOsobaKomponent
    {
        private string Imie;
        private string Plec;
        private string Zainteresowania;
        private int Ranking;
        private int RankingSuma = 0;

        public string PobierzImie()
        {
            return Imie;
        }

        public string PobierzPlec()
        {
            return Plec;
        }

        public string PobierzZainteresowania()
        {
            return Zainteresowania;
        }

        public int PobierzRankingUrody()
        {
            if (RankingSuma == 0)
            {
                return 0;
            }
            return (Ranking / RankingSuma);
        }

        public void UstawImie(string Imie)
        {
            this.Imie = Imie;
        }

        public void UstawPlec(string Plec)
        {
            this.Plec = Plec;
        }

        public void UstawZainteresowania(string Zainteresowania)
        {
            this.Zainteresowania = Zainteresowania;
        }

        public void UstawRankingUrody(int Ranking)
        {
            this.Ranking += Ranking;
            RankingSuma++;
        }
    }
}

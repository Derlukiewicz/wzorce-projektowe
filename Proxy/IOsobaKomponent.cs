﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    public interface IOsobaKomponent
    {
        string PobierzImie();
        string PobierzPlec();
        string PobierzZainteresowania();
        int PobierzRankingUrody();

        void UstawImie(string imie);
        void UstawPlec(string plec);
        void UstawZainteresowania(string zainteresowania);
        void UstawRankingUrody(int ranking);
    }
}

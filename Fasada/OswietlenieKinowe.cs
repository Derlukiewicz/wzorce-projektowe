﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fasada
{
	public class OswietlenieKinowe
	{
		public void Wlacz()
		{
			Console.WriteLine("Oświetlenie - włączone");
		}

		public void Wylacz()
		{
			Console.WriteLine("Oświetlenie - wyłączona");
		}

		public void Sciemniaj(int procent)
		{
			Console.WriteLine("Oświetlenie ściemnione na " + procent + "%");
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fasada
{
	public class Projektor
	{
		private OdtwarzaczDVD dvd;

		public Projektor(OdtwarzaczDVD dvd)
		{
			this.dvd = dvd;
		}

		public void Wlacz()
		{
			Console.WriteLine("Projektor włączony");
		}

		public void Wylacz()
		{
			Console.WriteLine("Projektor wyłączony");
		}

		public void TrybTV()
		{
			Console.WriteLine("Projektor w trybie TV");
		}

		public void TrybSzerokoekranowy()
		{
			Console.WriteLine("Projektor w trybie szerokoekranowym");
		}
	}
}

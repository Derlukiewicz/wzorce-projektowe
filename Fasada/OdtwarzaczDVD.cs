﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fasada
{
	public class OdtwarzaczDVD
	{
		private Wzmacniacz wzmacniacz;

		public OdtwarzaczDVD(Wzmacniacz wzmacniacz)
		{
			this.wzmacniacz = wzmacniacz;
		}

		public void Wlacz()
		{
			Console.WriteLine("Odtwarzacz DVD włączony");
		}

		internal void OdtwarzajFilm(string film)
		{
			Console.WriteLine("Odtwarzacz DVD film: " + film);
		}

		internal void Zatrzymaj()
		{
			Console.WriteLine("Odtwarzacz DVD stop");
		}

		internal void WysunDysk()
		{
			Console.WriteLine("Odtwarzacz DVD wysunięcie dysku");
		}

		internal void Wylacz()
		{
			Console.WriteLine("Odtwarzacz DVD wyłączony");
		}
	}
}

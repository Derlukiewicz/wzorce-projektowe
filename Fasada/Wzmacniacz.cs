﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fasada
{
	public class Wzmacniacz
	{
		private OdtwarzaczDVD dvd;
		private OdtwarzaczDVD cd;
		private Tuner tuner;

		public Wzmacniacz(Tuner tuner, OdtwarzaczCD cd, OdtwarzaczDVD dvd)
		{
			this.tuner = tuner;
			this.cd = dvd;
			this.dvd = dvd;
		}

		internal void Wlacz()
		{
			Console.WriteLine("Wzmacniacz włączony");
		}

		public OdtwarzaczDVD Dvd { get; set; }

		internal void UstawDzwiekPrzestrzenny()
		{
			Console.WriteLine("Wzmacniacz dźwięk przestrzenny");
		}

		internal void UstawGlosnosc(int p)
		{
			Console.WriteLine("Wzmacniacz głośność " + p + "%");
		}

		internal void Wylacz()
		{
			Console.WriteLine("Wzmacniacz wyłączony");
		}
	}
}

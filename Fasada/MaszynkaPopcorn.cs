﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fasada
{
	public class MaszynkaPopcorn
	{
		public void Wlacz()
		{
			Console.WriteLine("Maszynka do popcornu - włączona");
		}

		public void Wylacz()
		{
			Console.WriteLine("Maszynka do popcornu - wyłączona");
		}

		public void PrzygotujPopcorn()
		{
			Console.WriteLine("Maszynka przygotowuje popcorn");
		}
	}
}

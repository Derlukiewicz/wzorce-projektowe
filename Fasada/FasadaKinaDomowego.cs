﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fasada
{
	public class FasadaKinaDomowego
	{
		#region Pola
		Wzmacniacz wzmacniacz;
		Tuner tuner;
		OdtwarzaczDVD dvd;
		OdtwarzaczCD cd;
		Projektor projektor;
		OswietlenieKinowe oswietlenie;
		Ekran ekran;
		MaszynkaPopcorn popcorn; 
		#endregion

		#region Konstruktor
		public FasadaKinaDomowego(
			Wzmacniacz wzmacniacz,
			Tuner tuner,
			OdtwarzaczDVD dvd,
			OdtwarzaczCD cd,
			Projektor projektor,
			OswietlenieKinowe oswietlenie,
			Ekran ekran,
			MaszynkaPopcorn popcorn
			)
		{
			this.wzmacniacz = wzmacniacz;
			this.tuner = tuner;
			this.dvd = dvd;
			this.cd = cd;
			this.projektor = projektor;
			this.oswietlenie = oswietlenie;
			this.ekran = ekran;
			this.popcorn = popcorn;
		} 
		#endregion

		public void OdtwaczanieFilmu(string film)
		{
			Console.WriteLine("#Przygotuj się na seans filmowy...");

			popcorn.Wlacz();
			popcorn.PrzygotujPopcorn();

			oswietlenie.Sciemniaj(10);

			ekran.WDol();

			projektor.Wlacz();
			projektor.TrybSzerokoekranowy();

			wzmacniacz.Wlacz();
			wzmacniacz.Dvd = dvd;
			wzmacniacz.UstawDzwiekPrzestrzenny();
			wzmacniacz.UstawGlosnosc(5);

			dvd.Wlacz();
			dvd.OdtwarzajFilm(film);
		}

		public void KoniecFilmu()
		{
			Console.WriteLine("#Koniec seansu, wyłączanie kina domowego...");

			popcorn.Wylacz();

			oswietlenie.Wlacz();

			ekran.WGore();

			projektor.Wylacz();

			wzmacniacz.Wylacz();

			dvd.Zatrzymaj();
			dvd.WysunDysk();
			dvd.Wylacz();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fasada
{
	class Program
	{
		static Wzmacniacz wzmacniacz;
		static Tuner tuner;
		static OdtwarzaczDVD dvd;
		static OdtwarzaczCD cd;
		static Projektor projektor;
		static OswietlenieKinowe oswietlenie;
		static Ekran ekran;
		static MaszynkaPopcorn popcorn;

		static void Main(string[] args)
		{
			wzmacniacz = new Wzmacniacz(tuner, cd, dvd);
			tuner = new Tuner(wzmacniacz);
			dvd = new OdtwarzaczDVD(wzmacniacz);
			cd = new OdtwarzaczCD(wzmacniacz);
			projektor = new Projektor(dvd);
			oswietlenie = new OswietlenieKinowe();
			ekran = new Ekran();
			popcorn = new MaszynkaPopcorn();

			FasadaKinaDomowego kinoDomowe = new FasadaKinaDomowego(
				wzmacniacz,
				tuner,
				dvd,
				cd,
				projektor,
				oswietlenie,
				ekran,
				popcorn
				);

			kinoDomowe.OdtwaczanieFilmu("Obce szczęki");
			kinoDomowe.KoniecFilmu();

			Console.ReadKey();
		}
	}
}

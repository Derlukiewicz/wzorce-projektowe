﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kompozyt
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuSkladnik dinerMenu = new Menu("MENU RESTAURACJA DINER", "Luch");
            MenuSkladnik uJackaMenu = new Menu("MENU U JACKA", "Obiady");
            MenuSkladnik deseryMenu = new Menu("MENU DESERÓW", "Desery");

            MenuSkladnik wszystkieMenu = new Menu("WSZYSTKIE MENU", "Połączenie menu");
            wszystkieMenu.Dodaj(dinerMenu);
            wszystkieMenu.Dodaj(uJackaMenu);
            wszystkieMenu.Dodaj(deseryMenu);

            dinerMenu.Dodaj(
                new PozycjaMenu(
                    "Spaghetti",
                    "Włoski makron z sosem marinara",
                    false,
                    3.89));
            dinerMenu.Dodaj(deseryMenu);

            deseryMenu.Dodaj(
                new PozycjaMenu(
                    "Szarlotka",
                    "Szarlotka z polewą waniliową",
                    true,
                    1.59));

            uJackaMenu.Dodaj(
                new PozycjaMenu(
                    "Warzywa",
                    "Warzywa gotowane na parze z dodatkiem brązowego ryżu",
                    true,
                    3.99));

            Kelnerka kelnerka = new Kelnerka(wszystkieMenu);
            Console.WriteLine(kelnerka.DrukujMenu());
            Console.WriteLine();

            Console.ReadKey();

            Console.WriteLine(kelnerka.DrukujMenuWeget());

            Console.ReadKey();
        }
    }
}

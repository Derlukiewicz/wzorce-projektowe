﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Kompozyt
{
    public class IteratorKompozytu : IEnumerator
    {
        private Stack stos = new Stack();
        private object ms;

        public IteratorKompozytu(IEnumerable menuSkladniki)
        {
            foreach (var item in menuSkladniki)
            {
                stos.Push(item);
            }
        }

        public object Current
        {
            get { return ms; }
        }

        public bool MoveNext()
        {
            if (stos.Count > 0)
            {
                ms = stos.Pop();

                if (ms.GetType() == typeof(Menu))
                {
                    foreach (var item in (MenuSkladnik)ms)
                    {
                        stos.Push(item);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}

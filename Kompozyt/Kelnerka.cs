﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kompozyt
{
    public class Kelnerka
    {
        MenuSkladnik wszystkieMenu;

        public Kelnerka(MenuSkladnik wszystkieMenu)
        {
            this.wszystkieMenu = wszystkieMenu;
        }

        public string DrukujMenu()
        {
            return wszystkieMenu.Drukuj();
        }

        public string DrukujMenuWeget()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in wszystkieMenu)
            {
                try
                {
                    if ((item as PozycjaMenu).JestWegetarianska())
                    {
                        sb.AppendLine((item as MenuSkladnik).Drukuj());
                    }
                }
                catch { }
            }

            return sb.ToString();
        }
    }
}

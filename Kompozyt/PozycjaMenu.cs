﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kompozyt
{
	public class PozycjaMenu : MenuSkladnik
	{
		private string nazwa;
		private string opis;
		private bool wegetarianska;
		private double cena;

		public PozycjaMenu(string nazwa, string opis, bool wegetarianska, double cena)
		{
			this.nazwa = nazwa;
			this.opis = opis;
			this.wegetarianska = wegetarianska;
			this.cena = cena;
		}

		public override string PobierzNazwa()
		{
			return nazwa;
		}

		public override string PobierzOpis()
		{
			return opis;
		}

		public override double PobierzCena()
		{
			return cena;
		}

		public override bool JestWegetarianska()
		{
			return wegetarianska;
		}

		public override string Drukuj()
		{
            StringBuilder sb = new StringBuilder();
            
			sb.AppendLine("#" + PobierzNazwa() + "#");
			sb.Append("Wegetariańska: ");
			sb.AppendLine(JestWegetarianska()?"Tak":"Nie");
			sb.AppendLine("Cena: " + PobierzCena() + "$");
			sb.AppendLine(PobierzOpis());

            return sb.ToString();
		}

        public override IEnumerator GetEnumerator()
        {
            return new IteratorPusty();
        }
    }
}

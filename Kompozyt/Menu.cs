﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Kompozyt
{
	public class Menu : MenuSkladnik
	{
		List<MenuSkladnik> menuSkladniki = new List<MenuSkladnik>();
		string nazwa;
		string opis;

		public Menu(string nazwa, string opis)
		{
			this.nazwa = nazwa;
			this.opis = opis;
		}

		public override string PobierzNazwa()
		{
			return nazwa;
		}

		public override string PobierzOpis()
		{
			return opis;
		}

		public override void Dodaj(MenuSkladnik menuSkladnik)
		{
			menuSkladniki.Add(menuSkladnik);
		}

		public override void Usun(MenuSkladnik menuSkladnik)
		{
			menuSkladniki.Remove(menuSkladnik);
		}

		public override MenuSkladnik PobierzPotomek(int i)
		{
			return menuSkladniki.ElementAtOrDefault(i) as MenuSkladnik;
		}

		public override string Drukuj()
		{
            StringBuilder sb = new StringBuilder();

			sb.AppendLine(PobierzNazwa() + ", " + PobierzOpis());
			sb.AppendLine("----------------------");

            foreach (MenuSkladnik item in menuSkladniki)
            {
                foreach (string s in item.Drukuj().Replace(Environment.NewLine, "\n").Split('\n'))
                {
                    sb.AppendLine("|\t" + s);
                }
            }

            return sb.ToString();
		}

        //Stack stos = new Stack();

        public override IEnumerator GetEnumerator()
        {
            return new IteratorKompozytu(menuSkladniki);

            //foreach (var item in menuSkladniki)
            //{
            //    stos.Push(item);
            //}
            
            //while (stos.Count > 0)
            //{
            //    var ms = stos.Pop();

            //    if (ms.GetType() == typeof(Menu))
            //    {
            //        foreach (var item in (MenuSkladnik)ms)
            //        {
            //            stos.Push(item); 
            //        }
            //    }

            //    yield return ms;
            //}
        }
    }
}

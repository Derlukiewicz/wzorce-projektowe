﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kompozyt
{
	public abstract class MenuSkladnik: IEnumerable
	{
		public virtual string PobierzNazwa()
		{
			throw new NotSupportedException();
		}

		public virtual string PobierzOpis()
		{
			throw new NotSupportedException();
		}

		public virtual double PobierzCena()
		{
			throw new NotSupportedException();
		}

		public virtual bool JestWegetarianska()
		{
			throw new NotSupportedException();
		}
		

		public virtual void Dodaj(MenuSkladnik menuSkladnik)
		{
			throw new NotSupportedException();
		}

		public virtual void Usun(MenuSkladnik menuSkladnik)
		{
			throw new NotSupportedException();
		}

		public virtual MenuSkladnik PobierzPotomek(int i)
		{
			throw new NotSupportedException();
		}


		public virtual string Drukuj()
		{
			throw new NotSupportedException();
		}

        public abstract IEnumerator GetEnumerator();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyWcf.Stany
{
    public class NieMaMonety : StanMozliwy
    {
        AutomatSprzedajacy automatSprzedajacy;

        public NieMaMonety(AutomatSprzedajacy automatSprzedajacy)
        {
            this.automatSprzedajacy = automatSprzedajacy;
        }

        public void WlozMonete()
        {
            Console.WriteLine("Moneta przyjęta");
            automatSprzedajacy.UstawStan(automatSprzedajacy.Jest_Moneta);
        }

        public void ZwrocMonete()
        {
            Console.WriteLine("Nie włożyłeś monety");
        }

        public void PrzekrecGalke()
        {
            Console.WriteLine("Najpierw moneta");
        }

        public void Wydaj()
        {
            Console.WriteLine("Najpierw zapłać");
        }

        public override string ToString()
        {
            return "Automat oczekuje na monetę";
        }
    }
}

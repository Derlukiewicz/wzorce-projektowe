﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyWcf.Stany
{
    public interface StanMozliwy
    {
        void WlozMonete();
        void ZwrocMonete();
        void PrzekrecGalke();
        void Wydaj();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ProxyWcf
{
    [ServiceContract(Namespace = "AutomatSprzedajacy")]
    public interface IAutomatSprzedajacy
    {
        [OperationContract]
        string Lokalizacja();

        [OperationContract]
        int IloscTowaru();

        [OperationContract]
        string Stan();
    }
}

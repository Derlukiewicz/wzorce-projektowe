﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategia.Kwakanie;
using Strategia.Latanie;

namespace Strategia
{

	/// <summary>
	/// WZORZEC STRATEGIA
	/// </summary>

	public class Program
	{
		public static void Main(string[] args)
		{
			Kaczka dzikaKaczka = new DzikaKaczka();
			dzikaKaczka.WykonajKwacz();
			dzikaKaczka.WykonajLec();
			dzikaKaczka.Plywaj();
			dzikaKaczka.Wyswietl();

			Console.WriteLine();

			Kaczka model = new ModelKaczki();
			model.WykonajLec();
			model.UstawLatanieInterfejs(new LotZNapedemRakietowym());
			model.WykonajLec();

			Console.ReadLine();
		}
	}
}

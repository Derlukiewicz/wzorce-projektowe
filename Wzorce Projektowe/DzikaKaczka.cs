﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategia.Kwakanie;
using Strategia.Latanie;

namespace Strategia
{
	public class DzikaKaczka : Kaczka
	{
		public DzikaKaczka()
		{
			kwakanieInterfejs = new Kwacz();
			latanieInterfejs = new LatamBoMamSkrzydła();
		}

		public override void Wyswietl()
		{
			Console.WriteLine("Wygląda jak dzika kaczka");
		}
	}
}

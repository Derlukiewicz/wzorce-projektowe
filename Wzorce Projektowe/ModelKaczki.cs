﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategia.Kwakanie;
using Strategia.Latanie;

namespace Strategia
{
	public class ModelKaczki : Kaczka
	{
		public ModelKaczki()
		{
			latanieInterfejs = new NieLatam();
			kwakanieInterfejs = new Kwacz();
		}

		public override void Wyswietl()
		{
			Console.WriteLine("Jestem modelem kaczki");
		}
	}
}

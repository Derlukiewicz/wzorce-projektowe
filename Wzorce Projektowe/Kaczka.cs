﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategia.Kwakanie;
using Strategia.Latanie;


namespace Strategia
{
	public abstract class Kaczka
	{
		public KwakanieInterfejs kwakanieInterfejs;
		public LatanieInterfejs latanieInterfejs;

		public void UstawLatanieInterfejs(LatanieInterfejs li)
		{
			latanieInterfejs = li;
		}

		public void UstawKwakanieInterfejs(KwakanieInterfejs ki)
		{
			kwakanieInterfejs = ki;
		}

		public void WykonajKwacz()
		{
			kwakanieInterfejs.Kwacze();
		}

		public void WykonajLec()
		{
			latanieInterfejs.Lec();
		}

		public void Plywaj()
		{
			Console.WriteLine("Pływam ...");
		}

		public virtual void Wyswietl()
		{
			Console.WriteLine("Kaczka");
		}
	}

	//public class Kaczka
	//{
	//	public void Kwacz()
	//	{
	//		Console.WriteLine("Kwa kwa");
	//	}

	//	public void Plywaj()
	//	{
	//		Console.WriteLine("Plywam");
	//	}

	//	public void Lec()
	//	{
	//		Console.WriteLine("Lecę");
	//	}

	//	public virtual void Wyswietl()
	//	{
	//		Console.WriteLine("Kaczka");
	//	}
	//}
}

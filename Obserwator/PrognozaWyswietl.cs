﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obserwator
{
	public class PrognozaWyswietl : IObserwator, IWyswietlElement
	{
		private IPodmiot danePogodowe;
		private float temperatura;
		private float wilgotnosc;
		private float cisnienie;

		public PrognozaWyswietl(IPodmiot danePogodowe)
		{
			this.danePogodowe = danePogodowe;
		}

		public void Aktualizacja(float temperatura, float wilgotnosc, float cisnienie)
		{
			;
		}

		public void Wyswietl()
		{
			;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obserwator
{
	public interface IObserwator
	{
		void Aktualizacja(float temp, float wilgot, float cisnie);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obserwator
{
	public class DanePogodowe : IPodmiot
	{
		private List<IObserwator> obserwatorzy;
		private float temperatura;
		private float wilgotnosc;
		private float cisnienie;

		public DanePogodowe()
		{
			obserwatorzy = new List<IObserwator>(3);
		}

		public float PobierzTemperature()
		{
			return new Random(DateTime.Now.Millisecond).Next(-40, 40);
		}

		public float PobierzWilgotnosc()
		{
			return new Random(DateTime.Now.Millisecond).Next(100);
		}

		public float PobierzCisnienie()
		{
			return new Random(DateTime.Now.Millisecond).Next(800, 1300);
		}

		public void OdczytyZmiana()
		{
			PowiadomObserwatorow();
		}

		public void UstawOdczyty()
		{
			this.temperatura = PobierzTemperature();
			this.wilgotnosc = PobierzWilgotnosc();
			this.cisnienie = PobierzCisnienie();
			OdczytyZmiana();
		}

		public void ZarejestrujObserwatora(IObserwator o)
		{
			obserwatorzy.Add(o);
		}

		public void UsunObserwatora(IObserwator o)
		{
			obserwatorzy.Remove(o);
		}

		public void PowiadomObserwatorow()
		{
			foreach (var item in obserwatorzy)
			{
				item.Aktualizacja(temperatura, wilgotnosc, cisnienie);
			};
		}
	}
}

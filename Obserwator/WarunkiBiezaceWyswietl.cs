﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obserwator
{
	public class WarunkiBiezaceWyswietl : IObserwator, IWyswietlElement
	{
		private float temperatura;
		private float wilgotnosc;
		private IPodmiot danePogodowe;

		public WarunkiBiezaceWyswietl(IPodmiot DanePogodowe)
		{
			this.danePogodowe = DanePogodowe;
			DanePogodowe.ZarejestrujObserwatora(this);
		}

		public void Aktualizacja(float temperatura, float wilgotnosc, float cisnienie)
		{
			this.temperatura = temperatura;
			this.wilgotnosc = wilgotnosc;
			Wyswietl();
		}

		public void Wyswietl()
		{
			Console.WriteLine("Warunki bieżące " + temperatura + " stopni C oraz " + wilgotnosc + " % wilgotność");
		}
	}
}

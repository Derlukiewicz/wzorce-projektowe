﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obserwator
{
	class Program
	{
		static void Main(string[] args)
		{
			DanePogodowe danePogodowe = new DanePogodowe();
			WarunkiBiezaceWyswietl warunkiBiezaceWyswietl = new WarunkiBiezaceWyswietl(danePogodowe);
			StatystykaWyswietl statystykaWyswietl = new StatystykaWyswietl(danePogodowe);
			PrognozaWyswietl prognozaWyswietl = new PrognozaWyswietl(danePogodowe);
			IndeksCieplaWyswietl indeksCiepla = new IndeksCieplaWyswietl(danePogodowe);
			danePogodowe.UstawOdczyty();
			Console.ReadKey();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obserwator
{
	public interface IPodmiot
	{
		void ZarejestrujObserwatora(IObserwator o);

		void UsunObserwatora(IObserwator o);

		void PowiadomObserwatorow();
	}
}

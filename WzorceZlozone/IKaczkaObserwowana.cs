﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Obserwator

    public interface IKaczkaObserwowana
    {
        void Zarejestruj(IObserwator obserwator);
        void PowiadomObserwatorow();
    }
}

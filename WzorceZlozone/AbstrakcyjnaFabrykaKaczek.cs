﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Fabryka abstrakcujna

    public abstract class AbstrakcyjnaFabrykaKaczek
    {
        public abstract IKwaczaca UtworzDzikaKaczka();
        public abstract IKwaczaca UtworzPlaskonosKaczka();
        public abstract IKwaczaca UtworzWabikKaczka();
        public abstract IKwaczaca UtworzGumowaKaczka();
    }
}

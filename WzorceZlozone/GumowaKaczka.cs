﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class GumowaKaczka : IKwaczaca
    {
        Obserwowana obserw;

        public GumowaKaczka()
        {
            this.obserw = new Obserwowana(this);
        }

        public void Kwacz()
        {
            Console.WriteLine("Piszczę!");
            PowiadomObserwatorow();
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            obserw.Zarejestruj(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            obserw.PowiadomObserwatorow();
        }

        public override string ToString()
        {
            return "Gumowa kaczka";
        }
    }
}

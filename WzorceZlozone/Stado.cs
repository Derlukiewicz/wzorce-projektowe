﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Kompozyt

    public class Stado : IKwaczaca
    {
        List<IKwaczaca> ptaki = new List<IKwaczaca>();

        public void Dodaj(IKwaczaca ptak)
        {
            ptaki.Add(ptak);
        }

        public void Kwacz()
        {
            //Iterator

            foreach (var ptak in ptaki)
            {
                ptak.Kwacz();
            }
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            foreach (var ptak in ptaki)
            {
                ptak.Zarejestruj(obserwator);
            }
        }

        public void PowiadomObserwatorow()
        {
            foreach (var ptak in ptaki)
            {
                ptak.PowiadomObserwatorow();
            }
        }
    }
}

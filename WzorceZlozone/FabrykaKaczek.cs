﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Fabryka

    public class FabrykaKaczek : AbstrakcyjnaFabrykaKaczek
    {
        public override IKwaczaca UtworzDzikaKaczka()
        {
            return new DzikaKaczka();
        }

        public override IKwaczaca UtworzPlaskonosKaczka()
        {
            return new PlaskonosKaczka();
        }

        public override IKwaczaca UtworzWabikKaczka()
        {
            return new WabikKaczka();
        }

        public override IKwaczaca UtworzGumowaKaczka()
        {
            return new GumowaKaczka();
        }
    }
}

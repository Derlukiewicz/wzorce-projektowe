﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class PlaskonosKaczka : IKwaczaca
    {
        Obserwowana obserw;

        public PlaskonosKaczka()
        {
            this.obserw = new Obserwowana(this);
        }

        public void Kwacz()
        {
            Console.WriteLine("KWA! KWA!");
            PowiadomObserwatorow();
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            obserw.Zarejestruj(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            obserw.PowiadomObserwatorow();
        }

        public override string ToString()
        {
            return "Kaczka Płaskonos";
        }
    }
}

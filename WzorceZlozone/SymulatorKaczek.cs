﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class SymulatorKaczek
    {
        public void Uruchom(AbstrakcyjnaFabrykaKaczek fabrykaKaczek)
        {
            IKwaczaca plaskonosKaczka = fabrykaKaczek.UtworzPlaskonosKaczka();
            IKwaczaca wabikKaczka = fabrykaKaczek.UtworzWabikKaczka();
            IKwaczaca gumowaKaczka = fabrykaKaczek.UtworzGumowaKaczka();
            IKwaczaca ges = new GesAdapter(new Ges());

            Console.WriteLine("#Symulator Kaczek w stadze#");

            var stadoKaczek = new Stado();
            stadoKaczek.Dodaj(plaskonosKaczka);
            stadoKaczek.Dodaj(wabikKaczka);
            stadoKaczek.Dodaj(gumowaKaczka);
            stadoKaczek.Dodaj(ges);

            IKwaczaca dzikaKaczkaJeden = fabrykaKaczek.UtworzDzikaKaczka();
            IKwaczaca dzikaKaczkaDwa = fabrykaKaczek.UtworzDzikaKaczka();
            IKwaczaca dzikaKaczkaTrzy = fabrykaKaczek.UtworzDzikaKaczka();
            IKwaczaca dzikaKaczkaCztery = fabrykaKaczek.UtworzDzikaKaczka();

            var stadoDzikichKaczek = new Stado();
            stadoDzikichKaczek.Dodaj(dzikaKaczkaJeden);
            stadoDzikichKaczek.Dodaj(dzikaKaczkaDwa);
            stadoDzikichKaczek.Dodaj(dzikaKaczkaTrzy);
            stadoDzikichKaczek.Dodaj(dzikaKaczkaCztery);
            stadoKaczek.Dodaj(stadoDzikichKaczek);

            var ornitolog = new Ornitolog();
            stadoKaczek.Zarejestruj(ornitolog);

            Console.WriteLine("Całe stado:");
            Uruchom(stadoKaczek);

            Console.WriteLine("Stado dzikich kaczek:");
            Uruchom(stadoDzikichKaczek);

            Console.WriteLine("Kaczki kwaknęły " + KwakLicznik.IloscKwakniec() + " razy.");
        }

        public void Uruchom(IKwaczaca kaczka)
        {
            kaczka.Kwacz();
        }
    }
}

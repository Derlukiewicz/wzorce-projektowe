﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Fabryka

    public class FabrykaKaczekZLicznikiem : AbstrakcyjnaFabrykaKaczek
    {
        public override IKwaczaca UtworzDzikaKaczka()
        {
            return new KwakLicznik(new DzikaKaczka());
        }

        public override IKwaczaca UtworzPlaskonosKaczka()
        {
            return new KwakLicznik(new PlaskonosKaczka());
        }

        public override IKwaczaca UtworzWabikKaczka()
        {
            return new KwakLicznik(new WabikKaczka());
        }

        public override IKwaczaca UtworzGumowaKaczka()
        {
            return new KwakLicznik(new GumowaKaczka());
        }
    }
}

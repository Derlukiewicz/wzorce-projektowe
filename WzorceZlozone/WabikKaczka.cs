﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class WabikKaczka : IKwaczaca
    {
        Obserwowana obserw;

        public WabikKaczka()
        {
            this.obserw = new Obserwowana(this);
        }

        public void Kwacz()
        {
            Console.WriteLine("Kwak");
            PowiadomObserwatorow();
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            obserw.Zarejestruj(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            obserw.PowiadomObserwatorow();
        }

        public override string ToString()
        {
            return "Wabik";
        }
    }
}

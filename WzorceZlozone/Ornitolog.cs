﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class Ornitolog : IObserwator
    {
        public void Aktualizuj(IKaczkaObserwowana kaczka)
        {
            Console.WriteLine("Obserwowana " + kaczka.ToString() + " kwaknęła.");
        }
    }
}

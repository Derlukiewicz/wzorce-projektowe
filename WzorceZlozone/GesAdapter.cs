﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Adapter

    public class GesAdapter : IKwaczaca
    {
        Ges ges;
        Obserwowana obs;

        public GesAdapter()
        {
            obs = new Obserwowana(this);
        }

        public GesAdapter(Ges ges)
            : this()
        {
            this.ges = ges;
        }

        public void Kwacz()
        {
            ges.Gegaj();
            obs.PowiadomObserwatorow();
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            obs.Zarejestruj(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            obs.PowiadomObserwatorow();
        }

        public override string ToString()
        {
            return ges.ToString();
        }
    }
}

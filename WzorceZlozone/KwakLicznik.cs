﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class KwakLicznik : IKwaczaca
    {
        IKwaczaca kaczka;
        static int iloscKwakniec = 0;

        public KwakLicznik(IKwaczaca kaczka)
        {
            this.kaczka = kaczka;
        }

        public void Kwacz()
        {
            kaczka.Kwacz();
            iloscKwakniec++;
        }

        public static int IloscKwakniec()
        {
            return iloscKwakniec;
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            kaczka.Zarejestruj(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            kaczka.PowiadomObserwatorow();
        }
    }
}

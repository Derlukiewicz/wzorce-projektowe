﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstrakcyjnaFabrykaKaczek fabrykaKaczek = new FabrykaKaczekZLicznikiem();
            var symulatorKaczek = new SymulatorKaczek();

            symulatorKaczek.Uruchom(fabrykaKaczek);

            Console.ReadKey();
        }
    }
}

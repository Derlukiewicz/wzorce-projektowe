﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    public class DzikaKaczka : IKwaczaca
    {
        Obserwowana obserw;

        public DzikaKaczka()
        {
            this.obserw = new Obserwowana(this);
        }

        public void Kwacz()
        {
            Console.WriteLine("Kwa! Kwa!");
            PowiadomObserwatorow();
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            obserw.Zarejestruj(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            obserw.PowiadomObserwatorow();
        }

        public override string ToString()
        {
            return "Dzika kaczka";
        }
    }
}

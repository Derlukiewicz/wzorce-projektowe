﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WzorceZlozone
{
    //Obserwator

    public interface IObserwator
    {
        void Aktualizuj(IKaczkaObserwowana kaczka);
    }
}

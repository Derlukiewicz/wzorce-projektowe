﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WzorceZlozone
{
    //Obserwator

    public class Obserwowana : IKaczkaObserwowana
    {
        List<IObserwator> obserwatorzy = new List<IObserwator>();
        IKaczkaObserwowana kaczka;

        public Obserwowana(IKaczkaObserwowana kaczka)
        {
            this.kaczka = kaczka;
        }

        public void Zarejestruj(IObserwator obserwator)
        {
            obserwatorzy.Add(obserwator);
        }

        public void PowiadomObserwatorow()
        {
            foreach (var obserwator in obserwatorzy)
            {
                obserwator.Aktualizuj(kaczka);
            }
        }
    }
}

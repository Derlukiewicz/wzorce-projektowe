﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metoda_Szablonowa
{
	public class KawaZHaczykiem : NapojZKofeina
	{

		protected override bool CzyKlientChceDodatki()
		{
			ConsoleKey odpowiedz = PobierzOdpowiedz();
			Console.WriteLine();

			if(odpowiedz == ConsoleKey.T)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private ConsoleKey PobierzOdpowiedz()
		{
			Console.WriteLine("Czy dodać cukier i mleko do kawy (t/n)?");
			
			return Console.ReadKey().Key;
		}

		protected override void Zaparzenie()
		{
			Console.WriteLine("Zaparzenie i przesączanie kawy przez filtr");
		}

		protected override void DomieszanieDodatkow()
		{
			Console.WriteLine("Dodawanie cukru i mleka");
		}
	}
}

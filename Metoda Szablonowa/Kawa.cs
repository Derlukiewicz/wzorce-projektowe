﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metoda_Szablonowa
{
	public class Kawa : NapojZKofeina
	{
		protected override void Zaparzenie()
		{
			Console.WriteLine("Zaparzenie i przesączanie kawy przez filtr");
		}

		protected override void DomieszanieDodatkow()
		{
			Console.WriteLine("Dodawanie cukru i mleka");
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metoda_Szablonowa
{
	class Program
	{
		static void Main(string[] args)
		{
			Herbata herbata = new Herbata();
			herbata.RecepturaParzenia();

			Console.WriteLine();

			KawaZHaczykiem kawaHaczyk = new KawaZHaczykiem();
			kawaHaczyk.RecepturaParzenia();

			Console.ReadKey();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metoda_Szablonowa
{
	public abstract class NapojZKofeina
	{
		public void RecepturaParzenia()
		{
			GotowanieWody();
			Zaparzenie();
			NalewanieDoFilizanki();
			if (CzyKlientChceDodatki())
			{
				DomieszanieDodatkow();
			}
		}

		protected virtual bool CzyKlientChceDodatki()
		{
			return true;
		}

		private void NalewanieDoFilizanki()
		{
			Console.WriteLine("Nalewanie do filiżanki");
		}

		protected abstract void Zaparzenie();

		private void GotowanieWody()
		{
			Console.WriteLine("Gotowanie wody");
		}

		protected abstract void DomieszanieDodatkow();
	}
}

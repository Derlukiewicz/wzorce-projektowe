﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metoda_Szablonowa
{
	public class Herbata : NapojZKofeina
	{
		protected override void Zaparzenie()
		{
			Console.WriteLine("Wkładanie torebki herbaty do wrzątku");
		}

		protected override void DomieszanieDodatkow()
		{
			Console.WriteLine("Dodawanie cytryny");
		}
	}
}

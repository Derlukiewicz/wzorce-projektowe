﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	public class IndykAdapter : Kaczka
	{
		Indyk indyk;

		public IndykAdapter(Indyk indyk)
		{
			this.indyk = indyk;
		}

		public void Kwacz()
		{
			indyk.Gulgocz();
		}

		public void Lataj()
		{
			for (int i = 0; i < 5; i++)
			{
				indyk.Lataj();
			}
		}
	}
}

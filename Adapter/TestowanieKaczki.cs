﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	public class TestowanieKaczki
	{
		public static void Test()
		{
			DzikaKaczka kaczka = new DzikaKaczka();
			DzikiIndyk indyk = new DzikiIndyk();
			Kaczka indykAdapter = new IndykAdapter(indyk);

			Console.WriteLine();
			Console.WriteLine("Indyk:");
			indyk.Gulgocz();
			indyk.Lataj();

			Console.WriteLine();
			Console.WriteLine("Kaczka:");
			TestujKaczke(kaczka);

			Console.WriteLine();
			Console.WriteLine("Indyk w skórze kaczki:");
			TestujKaczke(indykAdapter);
		}

		public static void TestujKaczke(Kaczka kaczka)
		{
			kaczka.Kwacz();
			kaczka.Lataj();
		}
	}
}

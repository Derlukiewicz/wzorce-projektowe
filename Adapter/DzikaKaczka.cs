﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	public class DzikaKaczka : Kaczka
	{
		public void Kwacz()
		{
			Console.WriteLine("Kwa! Kwa!");
		}

		public void Lataj()
		{
			Console.WriteLine("Lecę...");
		}
	}
}

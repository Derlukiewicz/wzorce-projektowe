﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	public class DzikiIndyk :Indyk
	{
		public void Gulgocz()
		{
			Console.WriteLine("Gulgulgulgul");
		}

		public void Lataj()
		{
			Console.WriteLine("Lecę... ale tylko na krótkich dystansach");
		}
	}
}

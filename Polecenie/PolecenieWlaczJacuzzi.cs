﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class PolecenieWlaczJacuzzi : Polecenie
	{
		private Jacuzzi jacuzzi;

		public PolecenieWlaczJacuzzi(Jacuzzi jacuzzi)
		{
			this.jacuzzi = jacuzzi;
		}

		public void Wykonaj()
		{
			jacuzzi.Wlacz();
		}

		public void Wycofaj()
		{
			jacuzzi.Wylacz();
		}
	}
}

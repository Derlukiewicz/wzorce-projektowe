﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class MakroPolecenieTest
	{
		public static void Test()
		{
			Swiatlo swiatlo = new Swiatlo("Salon");
			Tv tv = new Tv("Salon");
			WiezaStereo stereo = new WiezaStereo("Salon");
			Jacuzzi jacuzzi = new Jacuzzi();

			Polecenie wlaczSwiatlo = new PolecenieWlaczSwiatlo(swiatlo);
			Polecenie wlaczStereo = new PolecenieWiezaStereoWlaczCD(stereo);
			Polecenie wlaczTV = new PolecenieWlaczTV(tv);
			Polecenie wlaczJacuzzi = new PolecenieWlaczJacuzzi(jacuzzi);

			Polecenie wylaczSwiatlo = new PolecenieWylaczSwiatlo(swiatlo);
			Polecenie wylaczStereo = new PolecenieWiezaStereoWylacz(stereo);
			Polecenie wylaczTV = new PolecenieWylaczTV(tv);
			Polecenie wylaczJacuzzi = new PolecenieWylaczJacuzzi(jacuzzi);

			Polecenie[] wlaczImpreze = 
			{
				wlaczSwiatlo,
				wlaczStereo,
				wlaczTV,
				wlaczJacuzzi
			};

			Polecenie[] wylaczImpreze =
			{
				wylaczJacuzzi,
				wylaczTV,
				wylaczStereo,
				wylaczSwiatlo
			};

			Polecenie makroWlaczImpreze = new MakroPolecenie(wlaczImpreze);
			Polecenie makroWylaczImpreze = new MakroPolecenie(wylaczImpreze);

			SuperPilot pilot = new SuperPilot();
			pilot.UstawPolecenie(0, makroWlaczImpreze, makroWylaczImpreze);

			Console.WriteLine(pilot);
			pilot.WcisnietoPrzyciskWlacz(0);
			pilot.WcisnietoPrzyciskWycofaj();
		}
	}
}

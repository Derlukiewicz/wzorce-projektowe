﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class MiniPilotTest
	{
		public static void Test()
		{
			MiniPilot pilot = new MiniPilot();
			Swiatlo swiatlo = new Swiatlo();
			PolecenieWlaczSwiatlo wlaczSwiatlo = new PolecenieWlaczSwiatlo(swiatlo);
			pilot.UstawPolecenie(wlaczSwiatlo);
			pilot.PrzyciskZostalNacisniety();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class SuperPilotWycofajTest
	{
		public static void Test()
		{
			SuperPilot pilotZWycofaniem = new SuperPilot();
			Swiatlo jadalniaSwiatlo = new Swiatlo("Jadalnia");

			Polecenie jadalniaWlaczSwiatlo = new PolecenieWlaczSwiatlo(jadalniaSwiatlo);
			Polecenie jadalniaWylaczSwiatlo = new PolecenieWylaczSwiatlo(jadalniaSwiatlo);

			pilotZWycofaniem.UstawPolecenie(0, jadalniaWlaczSwiatlo, jadalniaWylaczSwiatlo);

			pilotZWycofaniem.WcisnietoPrzyciskWlacz(0);
			pilotZWycofaniem.WcisnietoPrzyciskWylacz(0);
			
			Console.WriteLine();
			Console.WriteLine(pilotZWycofaniem);
			Console.WriteLine();

			pilotZWycofaniem.WcisnietoPrzyciskWycofaj();
			pilotZWycofaniem.WcisnietoPrzyciskWylacz(0);
			pilotZWycofaniem.WcisnietoPrzyciskWlacz(0);

			Console.WriteLine();
			Console.WriteLine(pilotZWycofaniem);
			Console.WriteLine();

			pilotZWycofaniem.WcisnietoPrzyciskWycofaj();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class PolecenieWylaczSwiatlo : Polecenie
	{
		Swiatlo swiatlo;

		public PolecenieWylaczSwiatlo(Swiatlo swiatlo)
		{
			this.swiatlo = swiatlo;
		}

		public void Wykonaj()
		{
			swiatlo.Wylacz();
		}

		public override string ToString()
		{
			return swiatlo.ToString() + " - Wyłącz";
		}


		public void Wycofaj()
		{
			swiatlo.Wlacz();
		}
	}
}

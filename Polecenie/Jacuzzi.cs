﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class Jacuzzi
	{
		internal void Wlacz()
		{
			Console.WriteLine("Jacuzzi włączone");
		}

		internal void Wylacz()
		{
			Console.WriteLine("Jacuzzi wyłączone");
		}

		public override string ToString()
		{
			return "Jacuzzi";
		}
	}
}

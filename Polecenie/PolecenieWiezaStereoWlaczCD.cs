﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class PolecenieWiezaStereoWlaczCD : Polecenie
	{
		WiezaStereo wiezaStereo;

		public PolecenieWiezaStereoWlaczCD(WiezaStereo wiezaStereo)
		{
			this.wiezaStereo = wiezaStereo;
		}

		public void Wykonaj()
		{
			wiezaStereo.Wlacz();
			wiezaStereo.UstawCD();
			wiezaStereo.UstawGlosnosc(11);
		}

		public override string ToString()
		{
			return wiezaStereo.ToString() + " - Włącz";
		}


		public void Wycofaj()
		{
			wiezaStereo.Wylacz();
		}
	}
}

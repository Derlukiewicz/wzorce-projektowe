﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class PolecenieWentylatorSufitowyNisko : Polecenie
	{
		WentylatorSufitowy wentylator;
		WentylatorSufitowyPredkosc poprzedniaPredkosc;

		public PolecenieWentylatorSufitowyNisko(WentylatorSufitowy wentylatorSufitowy)
		{
			this.wentylator = wentylatorSufitowy;
		}

		public void Wykonaj()
		{
			poprzedniaPredkosc = wentylator.Predkosc;
			wentylator.SrednieObroty();
		}

		public void Wycofaj()
		{
			switch (poprzedniaPredkosc)
			{
				case WentylatorSufitowyPredkosc.WYLACZ:
					wentylator.Wylacz();
					break;
				case WentylatorSufitowyPredkosc.WOLNO:
					wentylator.NiskieObroty();
					break;
				case WentylatorSufitowyPredkosc.SREDNIO:
					wentylator.SrednieObroty();
					break;
				case WentylatorSufitowyPredkosc.SZYBKO:
					wentylator.WysokieObroty();
					break;
			}
		}

		public override string ToString()
		{
			return wentylator.ToString() + ": Niskie Obroty";
		}
	}
}

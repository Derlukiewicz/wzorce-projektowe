﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class Swiatlo
	{
		private string pomieszczenie;

		public Swiatlo()
		{
		}

		public Swiatlo(string pomieszczenie)
		{
			this.pomieszczenie = pomieszczenie;
		}

		internal void Wlacz()
		{
			Console.WriteLine(pomieszczenie + " światło włączone");
		}

		internal void Wylacz()
		{
			Console.WriteLine(pomieszczenie + " światło wyłączone");
		}

		public override string ToString()
		{
			return "Światło w " + pomieszczenie;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class PolecenieWylaczTV : Polecenie
	{
		private Tv tv;

		public PolecenieWylaczTV(Tv tv)
		{
			this.tv = tv;
		}

		public void Wykonaj()
		{
			tv.Wylacz();
		}

		public void Wycofaj()
		{
			tv.Wlacz();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class SuperPilot
	{
		Polecenie[] poleceniaWlacz;
		Polecenie[] poleceniaWylacz;
		Polecenie polecenieWycofaj;

		public SuperPilot()
		{
			poleceniaWlacz = new Polecenie[7];
			poleceniaWylacz = new Polecenie[7];

			Polecenie brakPolaczenia = new BrakPolaczenia();
			for (int i = 0; i < 7; i++)
			{
				poleceniaWlacz[i] = brakPolaczenia;
				poleceniaWylacz[i] = brakPolaczenia;
			}
			polecenieWycofaj = brakPolaczenia;
		}

		public void UstawPolecenie(int slot, Polecenie polecenieWlacz, Polecenie polecenieWylacz)
		{
			poleceniaWlacz[slot] = polecenieWlacz;
			poleceniaWylacz[slot] = polecenieWylacz;
		}

		public void WcisnietoPrzyciskWlacz(int slot)
		{
			poleceniaWlacz[slot].Wykonaj();
			polecenieWycofaj = poleceniaWlacz[slot];
		}

		public void WcisnietoPrzyciskWylacz(int slot)
		{
			poleceniaWylacz[slot].Wykonaj();
			polecenieWycofaj = poleceniaWylacz[slot];
		}

		public void WcisnietoPrzyciskWycofaj()
		{
			polecenieWycofaj.Wycofaj();
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("------SuperPilot------");
			for (int i = 0; i < 7; i++)
			{
				sb.AppendLine("Slot(" + i + ") " + poleceniaWlacz[i].ToString() + " # " + poleceniaWylacz[i].ToString());
			}
			sb.AppendLine("Wycofaj " + polecenieWycofaj.ToString());
			return sb.ToString();
		}
	}
}

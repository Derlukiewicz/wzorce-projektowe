﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class PolecenieWlaczSwiatlo :Polecenie
	{
		Swiatlo swiatlo;

		public PolecenieWlaczSwiatlo(Swiatlo swiatlo)
		{
			this.swiatlo = swiatlo;
		}

		public void Wykonaj()
		{
			swiatlo.Wlacz();
		}

		public override string ToString()
		{
			return swiatlo.ToString() + " - Włącz";
		}


		public void Wycofaj()
		{
			swiatlo.Wylacz();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class PolecenieWiezaStereoWylacz : Polecenie
	{
		WiezaStereo wiezaStereo;

		public PolecenieWiezaStereoWylacz(WiezaStereo wiezaStereo)
		{
			this.wiezaStereo = wiezaStereo;
		}

		public void Wykonaj()
		{
			wiezaStereo.Wylacz();
		}

		public override string ToString()
		{
			return wiezaStereo.ToString() + " - Wyłącz";
		}


		public void Wycofaj()
		{
			wiezaStereo.Wlacz();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class MakroPolecenie : Polecenie
	{
		Polecenie[] polecenia;

		public MakroPolecenie(Polecenie[] polecenia)
		{
			this.polecenia = polecenia;
		}

		public void Wykonaj()
		{
			foreach (Polecenie polecenie in polecenia)
			{
				polecenie.Wykonaj();
			}
		}

		public void Wycofaj()
		{
			foreach (Polecenie polecenie in polecenia)
			{
				polecenie.Wycofaj();
			}
		}

		public override string ToString()
		{
			return "Makro polecenie";
		}
	}
}

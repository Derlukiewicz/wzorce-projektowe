﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class SuperPilotTest
	{
		public static void Test()
		{
			SuperPilot pilot = new SuperPilot();

			Swiatlo jadalniaSwiatlo = new Swiatlo("Jadalnia");
			Swiatlo kuchniaSwiatlo = new Swiatlo("Kuchnia");

			WiezaStereo wieza = new WiezaStereo("Jadalnia");

			Polecenie jadalniaWlaczSwiatlo = new PolecenieWlaczSwiatlo(jadalniaSwiatlo);
			Polecenie jadalniaWylaczSwiatlo = new PolecenieWylaczSwiatlo(jadalniaSwiatlo);
			Polecenie kuchniaWlaczSwiatlo = new PolecenieWlaczSwiatlo(kuchniaSwiatlo);
			Polecenie kuchniaWylaczSwiatlo = new PolecenieWylaczSwiatlo(kuchniaSwiatlo);
			Polecenie wiezaWlaczCD = new PolecenieWiezaStereoWlaczCD(wieza);
			Polecenie wiezaWylacz = new PolecenieWiezaStereoWylacz(wieza);

			pilot.UstawPolecenie(0, jadalniaWlaczSwiatlo, jadalniaWylaczSwiatlo);
			pilot.UstawPolecenie(1, kuchniaWlaczSwiatlo, kuchniaWylaczSwiatlo);
			pilot.UstawPolecenie(2, wiezaWlaczCD, wiezaWylacz);

			Console.WriteLine(pilot.ToString());

			Console.WriteLine();

			pilot.WcisnietoPrzyciskWlacz(0);
			pilot.WcisnietoPrzyciskWylacz(0);
			pilot.WcisnietoPrzyciskWlacz(1);
			pilot.WcisnietoPrzyciskWylacz(1);
			pilot.WcisnietoPrzyciskWlacz(2);
			pilot.WcisnietoPrzyciskWylacz(2);
			pilot.WcisnietoPrzyciskWlacz(3);
			pilot.WcisnietoPrzyciskWylacz(3);
		}
	}
}

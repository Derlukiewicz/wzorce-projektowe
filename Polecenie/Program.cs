﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Test miniPilot");
			MiniPilotTest.Test();
			Console.WriteLine("-------------------------------------------------------");

			Console.WriteLine("Test SuperPilot");
			SuperPilotTest.Test();
			Console.WriteLine("-------------------------------------------------------");

			SuperPilotWycofajTest.Test();
			Console.WriteLine("-------------------------------------------------------");

			SuperPilotWentylatorTest.Test();
			Console.WriteLine("-------------------------------------------------------");

			MakroPolecenieTest.Test();
			Console.WriteLine("-------------------------------------------------------");

			Console.ReadKey();
		}
	}
}

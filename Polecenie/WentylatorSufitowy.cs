﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public enum WentylatorSufitowyPredkosc
	{
		WYLACZ,
		WOLNO,
		SREDNIO,
		SZYBKO
	}

	public class WentylatorSufitowy
	{
		string lokalizacja;
		WentylatorSufitowyPredkosc predkosc;

		public WentylatorSufitowyPredkosc Predkosc 
		{
			get
			{
				return predkosc;
			}
		}

		public WentylatorSufitowy(string lokalizacja)
		{
			this.lokalizacja = lokalizacja;
			predkosc = WentylatorSufitowyPredkosc.WYLACZ;
		}

		public void Wylacz()
		{
			predkosc = WentylatorSufitowyPredkosc.WYLACZ;
			Console.WriteLine("Wentylator wyłączony");
		}

		public void NiskieObroty()
		{
			predkosc = WentylatorSufitowyPredkosc.WOLNO;
			Console.WriteLine("Wentylator włączony, niskie obroty");
		}

		public void SrednieObroty()
		{
			predkosc = WentylatorSufitowyPredkosc.SREDNIO;
			Console.WriteLine("Wentylator włączony, średnie obroty");
		}

		public void WysokieObroty()
		{
			predkosc = WentylatorSufitowyPredkosc.SZYBKO;
			Console.WriteLine("Wentylator włączony, wysokie obroty");
		}

		public override string ToString()
		{
			return lokalizacja + " wentylator sufitowy";
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class WiezaStereo
	{
		private string pomieszczenie;

		public WiezaStereo(string pomieszczenie)
		{
			this.pomieszczenie = pomieszczenie;
		}
		internal void Wylacz()
		{
			Console.WriteLine(pomieszczenie + " wieża stereo wyłączona");
		}

		internal void Wlacz()
		{
			Console.WriteLine(pomieszczenie + " wieża stereo włączona");
		}

		internal void UstawCD()
		{
			Console.WriteLine(pomieszczenie + " wieża stereo wybrano odtwarzacz CD");
		}

		internal void UstawGlosnosc(int skala)
		{
			Console.WriteLine(pomieszczenie + " wieża głośność ustawiona na " + skala);
		}

		public override string ToString()
		{
			return "Wieża Stereo w " + pomieszczenie;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class SuperPilotWentylatorTest
	{
		public static void Test()
		{
			SuperPilot pilot = new SuperPilot();
			
			WentylatorSufitowy wentylator = new WentylatorSufitowy("Gabinet");
			Polecenie wentylatorSrednio = new PolecenieWentylatorSufitowySrednio(wentylator);
			Polecenie wentylatorSzybko = new PolecenieWentylatorSufitowySzybko(wentylator);
			Polecenie wentylatorWylacz = new PolecenieWentylatorSufitowyWylacz(wentylator);

			pilot.UstawPolecenie(0, wentylatorSrednio, wentylatorWylacz);
			pilot.UstawPolecenie(1, wentylatorSzybko, wentylatorWylacz);

			pilot.WcisnietoPrzyciskWlacz(0);
			pilot.WcisnietoPrzyciskWylacz(0);
			
			Console.WriteLine();
			Console.WriteLine(pilot);
			Console.WriteLine();

			pilot.WcisnietoPrzyciskWycofaj();
			pilot.WcisnietoPrzyciskWlacz(1);

			Console.WriteLine();
			Console.WriteLine(pilot);
			Console.WriteLine();

			pilot.WcisnietoPrzyciskWycofaj();
		}
	}
}
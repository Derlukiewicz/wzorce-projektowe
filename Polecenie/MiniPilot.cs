﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polecenie
{
	public class MiniPilot
	{
		Polecenie slot;

		public MiniPilot()
		{

		}

		public void UstawPolecenie(Polecenie polecenie)
		{
			slot = polecenie;
		}

		public void PrzyciskZostalNacisniety()
		{
			slot.Wykonaj();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class Tv
	{
		private string pomieszczenie;

		public Tv(string pomieszczenie)
		{
			this.pomieszczenie = pomieszczenie;
		}

		internal void Wlacz()
		{
			Console.WriteLine(pomieszczenie + " TV włączone");
		}

		internal void Wylacz()
		{
			Console.WriteLine(pomieszczenie + " TV wyłączone");
		}

		public override string ToString()
		{
			return "TV w " + pomieszczenie;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class PolecenieWlaczTV : Polecenie
	{
		private Tv tv;

		public PolecenieWlaczTV(Tv tv)
		{
			this.tv = tv;
		}

		public void Wykonaj()
		{
			tv.Wlacz();
		}

		public void Wycofaj()
		{
			tv.Wylacz();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Polecenie
{
	public class PolecenieWylaczJacuzzi : Polecenie
	{
		private Jacuzzi jacuzzi;

		public PolecenieWylaczJacuzzi(Jacuzzi jacuzzi)
		{
			this.jacuzzi = jacuzzi;
		}

		public void Wykonaj()
		{
			jacuzzi.Wylacz();
		}

		public void Wycofaj()
		{
			jacuzzi.Wlacz();
		}
	}
}

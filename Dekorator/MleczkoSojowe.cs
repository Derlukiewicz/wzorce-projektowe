﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class MleczkoSojowe : SkladnikDekorator
	{
		private Napoj napoj;

		public MleczkoSojowe(Napoj napoj)
		{
			this.napoj = napoj;
		}

		public override string PobierzOpis()
		{
			return napoj.PobierzOpis() + ", mleczko sojowe";
		}

		public override double Koszt()
		{
			return napoj.Koszt() + 0.15;
		}
	}
}

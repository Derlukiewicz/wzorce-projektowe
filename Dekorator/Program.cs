﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	class Program
	{
		static void Main(string[] args)
		{
			Napoj napoj = new Espresso();
			Console.WriteLine(napoj.PobierzOpis() + " " + napoj.Koszt() + "zł");

			Napoj napoj2 = new MocnoPalona();
			napoj2 = new Czekolada(napoj2);
			napoj2 = new Czekolada(napoj2);
			napoj2 = new BitaSmietana(napoj2);
			Console.WriteLine(napoj2.PobierzOpis() + " " + napoj2.Koszt() + "zł");

			Napoj napoj3 = new StarCafeSpecial();
			napoj3 = new MleczkoSojowe(napoj3);
			napoj3 = new Czekolada(napoj3);
			napoj3 = new BitaSmietana(napoj3);
			Console.WriteLine(napoj3.PobierzOpis() + " " + napoj3.Koszt() + "zł");

			Console.ReadKey();
		}
	}
}
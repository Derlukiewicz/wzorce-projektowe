﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class Czekolada : SkladnikDekorator
	{
		private Napoj napoj;

		public Czekolada(Napoj napoj)
		{
			this.napoj = napoj;
		}

		public override string PobierzOpis()
		{
			return napoj.PobierzOpis() + ", czekolada";
		}

		public override double Koszt()
		{
			return napoj.Koszt() + 0.20;
		}
	}
}

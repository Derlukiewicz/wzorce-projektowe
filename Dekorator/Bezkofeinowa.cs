﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class Bezkofeinowa : Napoj
	{
		public Bezkofeinowa()
		{
			Opis = "Kawa Bezkofeinowa";
		}

		public override double Koszt()
		{
			return 1.05;
		}
	}
}

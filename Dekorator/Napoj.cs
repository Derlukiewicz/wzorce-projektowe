﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public abstract class Napoj
	{
		protected string Opis = "Napój nieznany";
		
		public virtual string PobierzOpis()
		{
			return Opis;
		}

		public abstract double Koszt();
	}
}

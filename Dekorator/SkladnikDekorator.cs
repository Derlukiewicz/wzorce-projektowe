﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public abstract class SkladnikDekorator : Napoj
	{
		public abstract override string PobierzOpis();
	}
}

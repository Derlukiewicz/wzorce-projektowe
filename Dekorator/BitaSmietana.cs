﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class BitaSmietana : SkladnikDekorator
	{
		private Napoj napoj;

		public BitaSmietana(Napoj napoj)
		{
			this.napoj = napoj;
		}

		public override string PobierzOpis()
		{
			return napoj.PobierzOpis() + ", bita śmietana";
		}

		public override double Koszt()
		{
			return napoj.Koszt() + 0.10;
		}
	}
}

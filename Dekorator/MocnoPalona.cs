﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class MocnoPalona : Napoj
	{
		public MocnoPalona()
		{
			Opis = "Kawa Mocno Palona";
		}

		public override double Koszt()
		{
			return 0.99;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dekorator;

namespace Dekorator
{
	public class StarCafeSpecial : Napoj
	{
		public StarCafeSpecial()
		{
			Opis = "Kawa Star Cafe Special";
		}

		public override double Koszt()
		{
			return 0.89;
		}
	}
}

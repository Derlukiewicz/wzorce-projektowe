﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class Mleko : SkladnikDekorator
	{
		private Napoj napoj;

		public Mleko (Napoj napoj)
		{
			this.napoj = napoj;
		}

		public override string PobierzOpis()
		{
			return napoj.PobierzOpis() + ", mleko";
		}

		public override double Koszt()
		{
			return napoj.Koszt() + 0.10;
		}
	}
}

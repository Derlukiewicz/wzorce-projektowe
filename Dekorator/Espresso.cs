﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator
{
	public class Espresso : Napoj
	{
		public Espresso()
		{
			Opis = "Kawa Espresso";
		}

		public override double Koszt()
		{
			return 1.99;
		}
	}
}

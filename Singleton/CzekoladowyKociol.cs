﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
	public sealed class CzekoladowyKociol
	{
		private static CzekoladowyKociol kociol;
		private static object padlock = new object();


		public static CzekoladowyKociol Kociol
		{
			get
			{
				lock (padlock)
				{
					if (kociol == null)
					{
						kociol = new CzekoladowyKociol();
					}
					return kociol;
				}
			}
		}

		private bool pusty;
		private bool ugotowany;

		private CzekoladowyKociol()
		{
			pusty = true;
			ugotowany = false;
			Console.WriteLine("Wywołanie kontruktora");
		}

		public void Napełnij()
		{
			if (JestPusty)
			{
				pusty = false;
				ugotowany = false;
				Console.WriteLine("Napełnianie bojlera mieszanką mleka i czekolady");
			}
		}

		public void Gotuj()
		{
			if (!JestPusty && !JestUgotowany)
			{
				ugotowany = true;
				Console.WriteLine("Gotowanie zawartość kotła");
			}
		}

		public void Oproznij()
		{
			if (!JestPusty && JestUgotowany)
			{
				pusty = true;
				Console.WriteLine("Opróżnianie bojlera z ugotowanej mieszanki");
			}
		}

		private bool JestUgotowany
		{
			get
			{
				return ugotowany;
			}
		}

		private bool JestPusty
		{
			get
			{
				return pusty;
			}
		}
	}
}

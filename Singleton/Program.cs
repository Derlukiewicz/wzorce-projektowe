﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
	class Program
	{
		static void Main(string[] args)
		{
			CzekoladowyKociol kociol = CzekoladowyKociol.Kociol;

			kociol.Napełnij();
			kociol.Napełnij();
			kociol.Gotuj();
			kociol.Oproznij();

			Console.ReadKey();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
	public sealed class Singleton
	{
		private static Singleton unikalnaInstancja;
		private static readonly object padlock = new object();

		private Singleton()
		{

		}

		//1. Jeżeli wydajność działania metody pobierzInstancję nie jest krytyczne dla sprawnego funcjonowania całej aplikacji, po prostu nie rób nic.

		public static Singleton PobierzInstancje()
		{
			lock (padlock)
			{
				if (unikalnaInstancja == null)
				{
					unikalnaInstancja = new Singleton();
				}
				return unikalnaInstancja;
			}
		}

		//2. Dokonaj modyfikacji kodu tak, aby żądany obiekt był tworzony z wyprzedzeniem, zamiast z opóźnieniem.

		//private static readonly Singleton unikalnaInstancja = new Singleton();

		//public static Singleton PobierzInstancje()
		//{
		//	return unikalnaInstancja;
		//}
	}
}

Zidentyfikuj fragmenty aplikacji, kt�re si� zmieniaj� i oddziel je od tych, kt�re pozostaj� sta�e.

Skoncentruj si� na tworzeniu interfejs�w, a nie implementacji.

Przek�adaj kompozycj� nad dziedziczenie.

Staraj si� tworzy� projekty, w kt�rych obiekty s� ze sob� lu�no powi�zane i, o ile to mo�liwe, nie oddzia�uj� na siebie wzajemnie.

Klasy powinny by� otwarte na rozbudow�, ale zamkni�te na modyfikacj�.

Uzale�niaj kod od abstrakcji, a nie od klas rzeczywistych.

Regu�a ograniczonej interakcji � rozmawiaj tylko z najbli�szymi przyjaci�mi.

Regu�a Hollywood � Nie dzwo� do nas, my zadzwonimy do Ciebie.

Klasa powinna mie� tylko jeden pow�d do zmian. - Kohezja.



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Proxy_Virtual
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri url = new Uri("http://randomburnham.files.wordpress.com/2013/03/random-havaianas.jpg");
            new Thread(() => System.Windows.Forms.Application.Run(new ImageForm(url))).Start();

            Console.ReadKey();
        }
    }
}

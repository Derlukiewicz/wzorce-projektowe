﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proxy_Virtual
{
    public partial class ImageForm : Form
    {
        Posrednik p;

        public ImageForm(Uri url)
        {
            InitializeComponent();
            p = new Posrednik(url, this);
            RefreshPictureBox();
        }

        internal void RefreshPictureBox()
        {
            pictureBox1.Image = p.Image;
            pictureBox1.Refresh();
        }
    }
}

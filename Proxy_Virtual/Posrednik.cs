﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using System.Net;

namespace Proxy_Virtual
{
    public class Posrednik
    {
        ImageForm imageForm;
        BackgroundWorker bw;
        Image _image;
        bool pobierany = false;

        public Image Image
        {
            get
            {
                {
                    if (!pobierany)
                    {
                        pobierany = true;
                        bw.RunWorkerAsync();
                    }

                    return _image;
                }
            }
        }

        int width = 400;
        int height = 400;
        private Uri url;

        public Posrednik(Uri url, ImageForm imageForm)
        {
            this.url = url;
            this.imageForm = imageForm;
            bw = new BackgroundWorker();
            bw.DoWork += bw_DoWork;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            GenerateImageTmp();
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {            
            imageForm.RefreshPictureBox();
        }

        private void GenerateImageTmp()
        {
            _image = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(_image))
            {
                string text = "Trwa ładowanie obrazka...";
                Point center = new Point(0, height / 2);
                g.DrawString(text, new Font("Arial", 16f), Brushes.Fuchsia, center);
            }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            using (WebClient wc = new WebClient())
            {
                _image = Image.FromStream(wc.OpenRead(url));
            }
        }

    }
}

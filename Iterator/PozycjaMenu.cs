﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	public class PozycjaMenu
	{
		public string nazwa { get; private set; }
		public string opis { get; private set; }
		public bool wegetarianska { get; private set; }
		public double cena { get; private set; }

		public PozycjaMenu(string nazwa, string opis, bool wegetarianska, double cena)
		{
			this.cena = cena;
			this.nazwa = nazwa;
			this.opis = opis;
			this.wegetarianska = wegetarianska;
		}
	}
}

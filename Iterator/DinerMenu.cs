﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	public class DinerMenu : IMenu
	{
		const int MAKS_LICZBA_ELEMENTOW = 6;
		int liczbaElementow = 0;
		PozycjaMenu[] pozycjaMenu;

		public DinerMenu()
		{
			pozycjaMenu = new PozycjaMenu[MAKS_LICZBA_ELEMENTOW];

			DodajElement("Wegetarianska kanapka BSP", "(Wegetarianski) Boczek z sałatą i pomidorem, chleb przenny pełnoziarnisty", true, 2.99);
			DodajElement("Kanapka BSP", "Boczek z sałatą i pomidorem, chleb przenny pełnoziarnisty", false, 2.99);
			DodajElement("Zupa dnia", "Zupa dnia i sałatka", false, 3.29);
			DodajElement("Hot-dog", "Hot-dog z kiszoną kapustą  dodatkiem sera", false, 3.05);
		}

		public void DodajElement(string nazwa, string opis, bool wegetarianska, double cena)
		{
			PozycjaMenu pozycja = new PozycjaMenu(nazwa, opis, wegetarianska, cena);

			if(liczbaElementow >= MAKS_LICZBA_ELEMENTOW)
			{
				Console.WriteLine("Niestety, menu jest pełne!");
			}
			else
			{
				pozycjaMenu[liczbaElementow] = pozycja;
				liczbaElementow++;
			}
		}

		public IIterator UtworzIterator()
		{
			return new DinnerMenuIterator(pozycjaMenu);
		}

		public IEnumerator UtworzIEnumerator()
		{
			return new DinerMenuIEnumerator(pozycjaMenu);
		}
	}
}

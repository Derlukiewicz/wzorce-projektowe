﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	public class PancakeMenu : IMenu
	{
		List<PozycjaMenu> pozycjeMenu;

		public PancakeMenu()
		{
			pozycjeMenu = new List<PozycjaMenu>();

			DodajElement("Śniadanie naleśnikowe K&B", "Naleśniki z jajecznicą i tostem", false, 2.99);
			DodajElement("Śniadanie naleśnikowe zwykłe", "Naleśniki z jajkiem sadzonym i kiełbasą", false, 2.99);
			DodajElement("Naleśniki z jagodami", "Naleśniki ze świeżymi jagodami", true, 3.49);
			DodajElement("Wafle nadziewane", "Wafle z jagodami lub truskawkami", true, 3.59);
		}

		public void DodajElement(string nazwa, string opis, bool wegetarianska, double cena)
		{
			PozycjaMenu pozycja = new PozycjaMenu(nazwa, opis, wegetarianska, cena);

			pozycjeMenu.Add(pozycja);
		}

		public IIterator UtworzIterator()
		{
			return new PancakeMenuIterator(pozycjeMenu);
		}

		public IEnumerator UtworzIEnumerator()
		{
			return pozycjeMenu.GetEnumerator();
		}
	}
}

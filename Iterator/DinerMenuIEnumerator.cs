﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iterator
{
	class DinerMenuIEnumerator : IEnumerator
	{
		private PozycjaMenu[] pozycjaMenu;
		private int pozycja = -1;

		public DinerMenuIEnumerator(PozycjaMenu[] pozycjaMenu)
		{
			this.pozycjaMenu = pozycjaMenu;
		}

		object IEnumerator.Current
		{
			get
			{
				return Current;
			}
		}

		public object Current
		{
			get
			{
				try
				{
					return pozycjaMenu.ElementAt(pozycja);
				}
				catch (IndexOutOfRangeException)
				{
					
					throw new InvalidOperationException();
				}
			}
		}

		public bool MoveNext()
		{
			pozycja++;
			if (pozycja >= pozycjaMenu.Length || pozycjaMenu[pozycja] == null)
			{
				return false;
			}
			else
			{				
				return true;
			}
		}

		public void Reset()
		{
			pozycja = -1;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	class Program
	{
		static void Main(string[] args)
		{
			DinerMenu dinerMenu = new DinerMenu();
			PancakeMenu pancakeMenu = new PancakeMenu();

			Kelnerka kelnerka = new Kelnerka(dinerMenu, pancakeMenu);
			kelnerka.DrukujMenu();

			Console.ReadKey();
		}
	}
}

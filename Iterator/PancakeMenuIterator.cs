﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	public class PancakeMenuIterator : IIterator
	{
		List<PozycjaMenu> pozycjaMenu;
		private int pozycja = 0;

		public PancakeMenuIterator(List<PozycjaMenu> dinerMenu)
		{
			this.pozycjaMenu = dinerMenu;
		}

		public bool HasNext()
		{
			if (pozycja >= pozycjaMenu.Count || pozycjaMenu.ElementAt(pozycja) == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public object Next()
		{
			PozycjaMenu pozMenu = pozycjaMenu.ElementAt(pozycja);
			pozycja++;
			return pozMenu;
		}

	}
}

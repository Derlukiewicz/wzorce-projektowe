﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	public class Kelnerka
	{
		IMenu pancakeMenu;
		IMenu dinerMenu;

		public Kelnerka(IMenu dinerMenu, IMenu pancakeMenu)
		{
			this.dinerMenu = dinerMenu;
			this.pancakeMenu = pancakeMenu;
		}

		public void DrukujMenu()
		{
			//IIterator dinerMenuIterator = dinerMenu.UtworzIterator();
			//IIterator pancakeMenuIterator = pancakeMenu.UtworzIterator();

			//Teraz korzystamy z wbudowanych iteraktorów

			IEnumerator dinerMenuIterator = dinerMenu.UtworzIEnumerator();
			IEnumerator pancakeMenuIterator = pancakeMenu.UtworzIEnumerator();

			Console.WriteLine("Śniadania:");
			DrukujMenu(pancakeMenuIterator);
			Console.WriteLine();
			Console.WriteLine("Obiady:");
			DrukujMenu(dinerMenuIterator);
		}

		private void DrukujMenu(IIterator iIterator)
		{
			while (iIterator.HasNext())
			{
				PozycjaMenu pozycja = iIterator.Next() as PozycjaMenu;
				Console.WriteLine(string.Format("\"{0}\" {1}${2}{3}", pozycja.nazwa, pozycja.cena, Environment.NewLine, pozycja.opis));
				Console.WriteLine();
			}
		}

		private void DrukujMenu(IEnumerator iEnumerator)
		{
			while (iEnumerator.MoveNext())
			{
				PozycjaMenu pozycja = iEnumerator.Current as PozycjaMenu;
				Console.WriteLine(string.Format("\"{0}\" {1}${2}{3}", pozycja.nazwa, pozycja.cena, Environment.NewLine, pozycja.opis));
				Console.WriteLine();
			}
		}
	}
}

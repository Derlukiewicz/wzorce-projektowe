﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
	public class DinnerMenuIterator : IIterator
	{
		PozycjaMenu[] elementy;
		int pozycja = 0;

		public DinnerMenuIterator (PozycjaMenu[] elementy)
		{
			this.elementy = elementy;
		}

		public bool HasNext()
		{
			if(pozycja >= elementy.Length || elementy[pozycja] == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public object Next()
		{
			PozycjaMenu pozycjaMenu = elementy[pozycja];
			pozycja++;
			return pozycjaMenu;
		}
	}
}

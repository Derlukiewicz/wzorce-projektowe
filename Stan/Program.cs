﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stan
{
    class Program
    {
        static void Main(string[] args)
        {
            var automat = new AutomatSprzedajacy(5);

            Console.WriteLine(automat.ToString());

            automat.WlozMonete();
            automat.PrzekrecGalke();

            Console.WriteLine();
            Console.WriteLine(automat.ToString());

            automat.WlozMonete();
            automat.ZwrocMonete();
            automat.PrzekrecGalke();

            Console.WriteLine();
            Console.WriteLine(automat.ToString());

            automat.WlozMonete();
            automat.PrzekrecGalke();
            automat.WlozMonete();
            automat.PrzekrecGalke();
            automat.ZwrocMonete();

            Console.WriteLine();
            Console.WriteLine(automat.ToString());

            automat.WlozMonete();
            automat.WlozMonete();
            automat.PrzekrecGalke();
            automat.WlozMonete();
            automat.PrzekrecGalke();
            automat.WlozMonete();
            automat.PrzekrecGalke();

            Console.WriteLine();
            Console.WriteLine(automat.ToString());

            Console.ReadKey();
        }
    }
}

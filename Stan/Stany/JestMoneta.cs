﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stan.Stany
{
    public class JestMoneta : StanMozliwy
    {
        private AutomatSprzedajacy automatSprzedajacy;
        private Random los = new Random();

        public JestMoneta(AutomatSprzedajacy automatSprzedajacy)
        {
            this.automatSprzedajacy = automatSprzedajacy;
        }

        public void WlozMonete()
        {
            Console.WriteLine("Nie można włożyć dodatkowej monety");
        }

        public void ZwrocMonete()
        {
            Console.WriteLine("Moneta zwrócona");
            automatSprzedajacy.UstawStan(automatSprzedajacy.NieMaMonety);
        }

        public void PrzekrecGalke()
        {
            Console.WriteLine("Obróciłeś gałkę...");
            int wygrana = los.Next(100);
            if (wygrana <= 10)
            {
                automatSprzedajacy.UstawStan(automatSprzedajacy.Promocja2w1);
            }
            else
            {
                automatSprzedajacy.UstawStan(automatSprzedajacy.TowarSprzedany);
            }
        }

        public void Wydaj()
        {
            Console.WriteLine("Nie wydano towaru");
        }

        public override string ToString()
        {
            return "Włożono monetę";
        }
    }
}

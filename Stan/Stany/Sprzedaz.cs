﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stan.Stany
{
    public class Sprzedaz : StanMozliwy
    {
        private AutomatSprzedajacy automatSprzedajacy;

        public Sprzedaz(AutomatSprzedajacy automatSprzedajacy)
        {
            this.automatSprzedajacy = automatSprzedajacy;
        }

        public void WlozMonete()
        {
            Console.WriteLine("Proszę czekać na wydanie towaru");
        }

        public void ZwrocMonete()
        {
            Console.WriteLine("Dokonano zakupu, nie mam mozliwość zwrotu monet");
        }

        public void PrzekrecGalke()
        {
            Console.WriteLine("Przekręcenie gałki nic już nie daje");
        }

        public void Wydaj()
        {
            automatSprzedajacy.ZwolnijGume();
            if (automatSprzedajacy.IloscTowaru > 0)
            {
                automatSprzedajacy.UstawStan(automatSprzedajacy.NieMaMonety);
            }
            else
            {
                Console.WriteLine("Brak towaru");
                automatSprzedajacy.UstawStan(automatSprzedajacy.BrakTowaru);
            }
        }

        public override string ToString()
        {
            return "Wydaje towar";
        }
    }
}

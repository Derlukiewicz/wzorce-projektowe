﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stan.Stany
{
    public class Brak : StanMozliwy
    {
        private AutomatSprzedajacy automatSprzedajacy;

        public Brak(AutomatSprzedajacy automatSprzedajacy)
        {
            this.automatSprzedajacy = automatSprzedajacy;
        }

        public void WlozMonete()
        {
            Console.WriteLine("Nie można włożyć monety, gdy jest brak towaru");
        }

        public void ZwrocMonete()
        {
            Console.WriteLine("Nic nie wydam - Brak towaru");
        }

        public void PrzekrecGalke()
        {
            Console.WriteLine("To nic nie da - Brak towaru");
        }

        public void Wydaj()
        {
            Console.WriteLine("Brak towaru");
        }

        public override string ToString()
        {
            return "Automat jest pusty";
        }
    }
}

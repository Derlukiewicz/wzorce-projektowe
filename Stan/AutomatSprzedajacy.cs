﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stan.Stany;

namespace Stan
{
    public class AutomatSprzedajacy
    {
        public StanMozliwy BrakTowaru { get; private set; }
        public StanMozliwy NieMaMonety { get; private set; }
        public StanMozliwy Jest_Moneta { get; private set; }
        public StanMozliwy TowarSprzedany { get; private set; }
        public StanMozliwy Promocja2w1 { get; private set; }

        StanMozliwy stan;
        int liczbaBatonikow = 0;

        public int IloscTowaru
        {
            get
            {
                return liczbaBatonikow;
            }
        }

        public AutomatSprzedajacy(int liczbaGum)
        {
            BrakTowaru = new Brak(this);
            NieMaMonety = new NieMaMonety(this);
            Jest_Moneta = new JestMoneta(this);
            TowarSprzedany = new Sprzedaz(this);
            Promocja2w1 = new Wygrana(this);

            UstawStan(BrakTowaru);

            this.liczbaBatonikow = liczbaGum;
            if (liczbaGum > 0)
            {
                stan = NieMaMonety;
            }
        }

        internal void UstawStan(StanMozliwy stanAutomatu)
        {
            stan = stanAutomatu;
        }  
        
        public void WlozMonete()
        {
            stan.WlozMonete();
        }

        public void ZwrocMonete()
        {
            stan.ZwrocMonete();
        }

        public void PrzekrecGalke()
        {
            stan.PrzekrecGalke();
            stan.Wydaj();
        }

        public void ZwolnijGume()
        {
            Console.WriteLine("Wypada batonik... ");
            if (liczbaBatonikow > 0)
            {
                liczbaBatonikow--;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Automat Z.O.O.");
            sb.AppendLine("Zapas: " + IloscTowaru);
            sb.AppendLine(stan.ToString());

            return sb.ToString();
        }
    }
}
